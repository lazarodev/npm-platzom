# Platzom

Platzom es un idioma inventando para el [curso de fundamentos de JavaScript de Platzi](https://platzi.com/js), el mejor lugar de 
educacion online.

## Descripcion del idioma

- aqui van la descripcion del idioma.

## Instalacion

npm install platzom

## Uso

import platzom from 'platzom'

platzom("Programar") // Program

## Creditos
- [Lazaro Dev](https://twitter.com/lazarodev)

## Licencia

[MIT](https://opensource.org/licenses/MIT)