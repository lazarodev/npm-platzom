
const expect = require('chai').expect
const platzom = require('..').default

describe('#platzom', function() {
    it('Test #1', function() {
        const translation = platzom("Programar")
        expect(translation).to.equal("Program")
    })
    it('Test #2', function() {
        const translation = platzom("Zorro")
        const translation2 = platzom("Zarpar")

        expect(translation).to.equal("Zorrope")
        expect(translation2).to.equal("Zarppe")
    })
    it('', function() {
        
    })
    it('', function() {
        
    })
})